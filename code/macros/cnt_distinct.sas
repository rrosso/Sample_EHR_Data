

%macro cnt_distinct(idvar=,grpvar=,dir=work,indata=,title=Number obs,where="");
     
    %let cond = %bquote(&where.);
    title2 "&title.";
	proc sql;
	    select 
        %if &grpvar ^=  %then %do;
		    &grpvar,
		%end;
        count(distinct &idvar) as num_&idvar.
		from   &dir..&indata.
		%if &cond ^= "" %then %do;
		    where  %unquote(&cond.)
		%end;
		%if &grpvar ^=  %then %do;
			group by &grpvar.
		%end;
        ;
	quit;
	title2;

%mend;

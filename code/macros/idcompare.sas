***************************************************************************
*******Program:      idcompare.sas
*******Description:  Compares id variables in two files against each other
*******Programmer:   Randy Rosso
*******Notes: 
***************************************************************************;

/*
%idcompare parameters:
dir1       =	name of directory holding first data set
filenm1    =  name of first data set
printvars1 =  additional variables to print from first data set
chkvar     =  variable to check
dir2       =  name of directory holding second data set
filenm2    =  name of second data set
printvars2 =  additional variables to print from second data set
print1     =  1 if want to print first 100 obs of all records in master file not in second file
		          omit this parameter if do not want to print these
print2     =  1 if want to print first 100 obs of all records in second file not in master file
		          omit this parameter if do not want to print these*/

%macro idcompare (dir1=work, filenm1=, printvars1=, print1=, chkvar=, dir2=work, filenm2=, printvars2=, print2= );

*generate comma-separated versions of printvars parameters, for SQL statements;
%let printvars1_dsd = ;
%let printvars2_dsd = ;

%if &printvars1. ^= %then %do;
    %let printvars1_dsd = %sysfunc(translate(&printvars1.,',',' '));
%end;

%if &printvars2. ^= %then %do;
    %let printvars2_dsd = %sysfunc(translate(&printvars2.,',',' '));
%end;

*test whether all ids in first file are in second file;
proc sql;
create table test_&chkvar. as
select * from &dir1..&filenm1
where not missing(&chkvar) and &chkvar not in (select &chkvar from &dir2..&filenm2);
quit;

*output number of observations of ids in first file not in second file;
proc contents data=test_&chkvar. noprint out=contents_data1_&chkvar. (keep=nobs);
run;

%local dset nobs;
   %let dset=test_&chkvar.;
   *open dataset;
   %let dsid = %sysfunc(open(&dset));
   %if &dsid %then
      %do;
         *assign number of obs to macro variable &nobs;
         %let nobs = %sysfunc(attrn(&dsid,NOBS));
         *close dataset;
         %let fc   = %sysfunc(close(&dsid));
      %end;
   %else %put Open for data set &dset failed - %sysfunc(sysmsg());

    *if no observations found, output message in log that passed test;
	%if &nobs = 0 %then %do;
		%put                                                          ;
		%put PASSED TEST - &nobs obs with &chkvar in &dir1..&filenm1, not in &dir2..&filenm2..;
		%put                                                          ;
		*output message in output destination (listing, ods) that passed test, showing 0 obs;
		proc print data=contents_data1_&chkvar. (obs=1);
		var nobs;
		title2 "PASSED TEST - 0 obs where &chkvar in &dir1..&filenm1 and not in &dir2..&filenm2..";
		run;
	%end;
	*if positive number of observations found, output message in log that failed test;
	%else %do;
		%put                                                          ;
		%put FAILED TEST - &nobs obs with &chkvar in &dir1..&filenm1., not in &dir2..&filenm2..;
		%put                                                          ;
		    *output message in output destination that failed test, showing number of obs;
			proc print data=contents_data1_&chkvar. (obs=1);
			var nobs;
			title2 "FAILED TEST - nobs = # obs with &chkvar in &dir1..&filenm1, not in &dir2..&filenm2..";
			run;

            *if user requests printing observations, print up to 100 obs of &chkvar + user-specified variables;
            %if &print1 = 1 %then %do;
			    proc print data=test_&chkvar. (obs=100);
			    var &chkvar &printvars1.;
			    title2 "FAILED TEST - first 100 obs where &chkvar in &dir1..&filenm1, not in &dir2..&filenm2..";
			    run;
			%end;
	%end;


%* test whether all ids in second file are in first file;
proc sql;
create table test2_&chkvar. as
select * from &dir2..&filenm2.
where &chkvar not in (select &chkvar from &dir1..&filenm1 );
quit;

*output number of observations of ids in second file not in first file;
proc contents data=test2_&chkvar. noprint out=contents_data2_&chkvar. (keep=nobs);
run;

%local dset2 nobs2;
   %let dset2=test2_&chkvar.;
   *open dataset;
   %let dsid2 = %sysfunc(open(&dset2));
   %if &dsid2 %then
      %do;
         *assign number of obs to macro variable &nobs;
         %let nobs2 =%sysfunc(attrn(&dsid2,NOBS));
         *close dataset;
         %let fc = %sysfunc(close(&dsid2));
      %end;
   %else
      %put Open for data set &dset2 failed - %sysfunc(sysmsg());

    *if no observations found, output message in log that passed test;
	%if &nobs2 = 0 %then %do;
		%put                                                          ;
		%put PASSED TEST - &nobs2 obs where &chkvar in &dir2..&filenm2. and not in &dir1..filenm1..;
		%put                                                          ;
		*output message in output destination (listing, ods) that passed test, showing 0 obs;
		proc print data=contents_data2_&chkvar. (obs=1);
		var nobs;
		title2 "PASSED TEST - 0 obs with &chkvar in &dir2..&filenm2., not in &dir1..&filenm1..";
		run;
	%end;
	*if positive number of observations found, output message in log that failed test;
	%else %do;
		%put                                                          ;
		%put FAILED TEST - &nobs2 obs where &chkvar in &dir2..&filenm2. and not in &dir1..&filenm1..;
		%put                                                          ;
		*output message in output destination that failed test, showing number of obs;
		proc print data=contents_data2_&chkvar. (obs=1);
		var nobs;
		title2 "FAILED TEST - nobs = # obs with &chkvar in &dir2..&filenm2., not in &dir1..&filenm1..";
		run;
		    *if user requests printing observations, print up to 100 obs of &chkvar + user-specified variables;
			%if &print2 = 1 %then %do;
				proc print data=test2_&chkvar. (obs=100);
				var &chkvar &printvars2.;
				title2 "FAILED TEST - first 100 obs with &chkvar in &dir2..&filenm2., not in &dir1..&filenm1..";
				run;
			%end;
	%end;
    title2;
%mend;

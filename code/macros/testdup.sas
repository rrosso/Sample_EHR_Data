******************************************************************************************;
*******Program:      testdup.sas 
*******project name: CCSD
*******Description:  This macro checks for duplicate records based on a key field(s) (idvars)
*******				       and prints out the first 100 duplicate records
*******Date Created: 2-10-2014
*******Date updated: 
*******Programmer:   Randy Rosso
*******Notes: 
******************************************************************************************;
   
*
*%testdup parameters:
*dir=		directory file is in (default is work)
*filenm= 	name of data file
*idvars= 	id variable(s) on which to check for duplicates
*keepvars=   variables to include in final duplicates report, in addition to idvars
*print=      turn on/off printing observations (1=on, any other value=off--on by default)
*;
   
   ** check for dups **;
   %MACRO testdup(dir=work, filenm= , idvars= , keepvars=, print=0 );

    ********************
    *** ER/ROR HANDLING
    ********************;
    
    %if %quote(&dir) = %str() or %quote(&filenm) = %str() or %quote(&idvars) = %str() %then %do;
        	%put >>>ER%str()ROR: At least one required parameter is missing. Please check macro specification ;
        	%goto EXIT;
    %end;
    	  
    ** create comma-separated version of &idvars and count number of variables listed in &idvars;
    %let byvars_dsd = %sysfunc(translate(&idvars, ',',' '));
	  %let numids = %sysfunc(countw(&idvars));
	  
  	%do i = 1 %to &numids;
	       %let id&i. = %scan(&idvars, &i., ' ');
	  %end;
	  
	  %let nexttolast = %eval(&numids. - 1);
    
    ** if any keepvars specified, create comma-separated version and count number of vars listed;
    %if %quote(&keepvars) ^= %str() %then %do;
        %let keepvars_dsd = %sysfunc(translate(&keepvars, ',',' '));
        %let numkeeps = %sysfunc(countw(&keepvars));
        %do i = 1 %to &numkeeps;
       	    %let keep&i. = %scan(&keepvars, &i., ' ');
	      %end;
	      %let nexttolast_keeps = %eval(&numkeeps. - 1);
    %end;
    %if %quote(&keepvars) = %str() %then %do;
    	  %let keepvars_dsd = %str();
    %end;
    
	       PROC SQL STIMER;
           CREATE TABLE testdup AS
           SELECT &byvars_dsd, COUNT(*) AS COUNT
           FROM &dir..&filenm.
		   WHERE cmiss(&byvars_dsd) = 0
           GROUP BY &byvars_dsd
           HAVING count > 1
           order by &byvars_dsd;
         quit;

   proc freq data=testdup;
   tables count;
   title2 "number of records with duplicate values of &idvars in &dir..&filenm ";
   run;


   /*results should be zero for unique records.
   if not, run second section to extract duplicate records */

  proc sql;

  create table testdup2 as
  select %do iter = 1 %to &numids;
             _a.&&id&iter.,
	     %end;
		 %if %quote(&keepvars) ^= %str() %then %do iter2 = 1 %to &numkeeps;
		     _a.&&keep&iter2.,
		 %end;
		 _b.count
  from &dir..&filenm. as _a, testdup as _b
  where %do iter3 = 1 %to &nexttolast;
            _a.&&id&iter3. = _b.&&id&iter3. and
		%end;
		_a.&&id&numids = _b.&&id&numids
  order by %do iter4 = 1 %to &nexttolast;
               _a.&&id&iter4.,
		   %end;
		   _a.&&id&numids.;

  quit;

  %if &print = 1 %then %do;
      proc print data=testdup2 (obs=100);
          title2 "duplicate records";
      run;
  %end;
  title2;

  *************************************
  *** Skip here if er/ror encountered 
  *************************************;
  %EXIT: ;
  
  %MEND;

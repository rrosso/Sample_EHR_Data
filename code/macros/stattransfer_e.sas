/*-----------------------------------------------------------------------------

   STATTRANSFER_V2_EXCEL.SAS

     *** Created by MJacobus on 7/08/08 (for use with Stattransfer V9)
     *** Updated by SLukashanets on 12/2/2013

     *** Created to easily and programmatically convert SAS files to Stata files (and vice versa)

     *** UPDATES

                10/20/08 - Modified to verify file was created
                         - Modified so that user does not have to create
                           a permanent SAS dataset to create a STATA file
                10/21/08 - Implementing check to make sure that, if a file exists,
                           it was actually created by this program
                06/04/09 - Added options to macro
                12/02/09 - Added macro parameter for stattransfer.exe location
                09/07/10 - Text file output so that program can be invoked by .do files
                10/19/10 - Changing so that timestamp can differ by 5 minutes
                01/03/10 - Creating _v1 and storing on T drive;
                           altered parameters a bit
                07/15/11 - _v2 changes:
                             (1) removed yntxt parameter and associated code
                             (2) reduced parameters from 7 to 3
                             (3) added er/ror handling to verify parameters are correct
		12/2/2012 - added Excel sheets + Access tables. 

     *** PARAMETERS

             input_ds  = Input data set
             output_ds = Output data set
	     sheet =  	 Data subdivision name (if blank, will take the first sheet/table)
             statpath  = Fully qualified path/filename of the Stattransfer executable file
                         (note: we should aim to have the Stattransfer installation standardized to this location)

             The input/output data set parameters will ALWAYS need to be specified and they ALWAYS have to include the
             current/targeted file extentions. There is some flexibility in how directories are specified; you can use a fully qualified
             directory, a libname, or leave as blank if the file is in the work directory (although work.dsname.ext will work also).

       Below are some examples of how to specify this macro:

            %stattransfer_e (input_ds = in.dsname.sas7bdat,
			    sheet=,
                           output_ds= out.dsname.dta) ;

            %stattransfer_e (input_ds = dsname.sas7bdat,
                              sheet=,
                           output_ds= out.dsname.dta) ;

            %stattransfer_e (input_ds = dsname.sas7bdat,
                             sheet=,
                           output_ds= D:\Dirname\dsname.dta) ;

     *** NOTE: If user wants to create proc format libary (stata-to-sas),
              they must do the following:

                1) In the stattransfer folder (dir where .exe file is located),
                    create a file called "PROFILE.STC"

                2) Open in a text editor, and add the text "set write-sas-fmts y".

                3) Additionally, Add the text "set read-sas-fmts y" to convert sas format
                   libraries into Stata value labels.

              Other set commands can be specified (see stattranfer help
              section on "command processor").

  ----------------------------------------------------------------------------- */

%macro stattransfer_e ( input_ds  = ,
                      output_ds = ,
			sheet = ,
                      statpath  = C:\Program Files\StatTransfer10\st.exe
                     );

  /*---Preserving inital options */
    %local _xwait _xsync _mprint input_ds_fname output_ds_fname libnamemac;
    %let _xwait  = %sysfunc(getoption(xwait));
    %let _xsync  = %sysfunc(getoption(xsync));
    %let _mprint = %sysfunc(getoption(mprint));

    options nomprint noxwait xsync ;

  /*---Parsing input_ds and output_ds */

  /*-----------------------*/
  /*--- Er/ror-Handling ---*/
  /*-----------------------*/

  /*--- First up, if any parameter is missing, we skip all er/ror handling below and end the macro */

  %if %quote(&input_ds.) = %str() or %quote(&output_ds.) = %str() or %quote(&statpath.) = %str() %then %do ;

      %put >>> ER%str()ROR: At least one parameter is missing, please check macro specification ;
      %goto EXIT;

  %end;

  %let paramERR = 0;

  %put ;
  %put >>> Parameter Checks >>>>>>>>>>>> ;

  /*--- %ERR_HANDLE_DS verifies that the input and output data sets are correctly specified */

  %macro err_handle_ds (dsname);

     /*Removing quotation marks (single and double) */
       %let &dsname. = %sysfunc(compress(&&&dsname..,"'"'"')) ; /*" - this preserves color-coding in the log*/

     /*Replacing forward with backward slashes*/
       %let &dsname. = %sysfunc(tranwrd(&&&dsname..,/,\)) ;

     /*---If filename does not have an extention specified, goto exit */

       %if %index(%upcase(&&&dsname..),.DTA) = 0
         & %index(%upcase(&&&dsname..),.SAS7BDAT) = 0
         & %index(%upcase(&&&dsname..),.SAV) = 0
         & %index(%upcase(&&&dsname..),.CSV) = 0
         & %index(%upcase(&&&dsname..),.XLS) = 0
         & %index(%upcase(&&&dsname..),.ACCDB) = 0
       %then %do ;

           %put >>> ER%str()ROR: User did not specify a valid file extention (.DTA, .SAS7BDAT, .SAV, .CSV, .XLS .ACCDB) for %nrstr(&)&dsname. ;
           %let paramERR = 1;

       %end;


     /*---If directory is explicitly specified, then use this information */

       %if %index(&&&dsname..,\) > 0 %then %let &dsname._fname = &&&dsname.. ;

     /*---Else If a libname is present, then derive the full directory name */

       %else %if %trim(%left(%scan(&&&dsname..,3,.))) ~= %str() %then %do ;

          /*---Verify that the libname is valid */

          %let libnamemac = %sysfunc(pathname(%left(%trim(%scan(&&&dsname..,1,.)))));

          %if %quote(&libnamemac.) = %str() %then %do ;

            %put >>> ER%str()ROR: Libname -%left(%trim(%scan(&&&dsname..,1,.)))- is not valid for %nrstr(&)&dsname. ;
            %let paramERR = 1;

          %end;
          %else %let &dsname._fname = %quote(&libnamemac.)\%substr(%left(%trim(&&&dsname..)),%eval(%length(%scan(%left(%trim(&&&dsname..)),1,.)))+2);

       %end;

      /*---If a libname is not present and a directory is not explicitly listed, setting pathname = work */

       %else %if %index(&&&dsname..,\) = 0 & %trim(%left(%scan(&&&dsname..,3,.))) = %str() %then %do ;

          %let &dsname._fname = %sysfunc(pathname(work))\%trim(%left(&&&dsname..));

       %end;

       %put >>> %nrstr(&)&dsname. = &&&dsname.. = &&&dsname._fname., %nrstr(&paramERR) = &paramERR. ;

  %mend;
  %err_handle_ds (input_ds);
  %err_handle_ds (output_ds);

  /*--- Removing quotes from statpath */

       %let statpath = %sysfunc(compress(&statpath.,"'"'"')) ; /*" - this preserves color-coding in the log*/

  /*--- Now we are checking that the executable Stattransfer file exists */

     %if %sysfunc(fileexist(%quote(&statpath.))) = 0 %then %do;

         %put >>> ER%str()ROR: The file %nrstr(&statpath.) = &statpath. does not exist ;
         %let paramERR = 1 ;

     %end;

     %put >>> %nrstr(&statpath.) = &statpath., %nrstr(&paramERR) = &paramERR. ;

  /*--- Now verifying that the input ds exists */

     %if %sysfunc(fileexist(%quote(&input_ds_fname.))) = 0 %then %do;

         %put >>> ER%str()ROR: The input dataset = &input_ds_fname. does not exist ;
         %let paramERR = 1 ;

     %end;

  /*--------------------------------------------------------------------------------*/
  /*--- If any er/ror occurs with parameter specification, goto the end of the macro */
  /*--------------------------------------------------------------------------------*/

     %if &paramERR = 1 %then %goto EXIT;
     %else %if &paramERR = 0 %then %put >>> All parameters defined correctly ;

     %put >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ;
     %put ;

 /*---(END) Er/ror Handling----------------*/


 /*---Stata files created as Stata/SE-*/
 /*   Note: this option formerly was required for large stata datasets. It may no longer apply to
      Stata 11 however it appears harmless so leaving as is for now */

   %if %upcase(%scan(&output_ds_fname.,-1,%str(.)))=%str(DTA) %then %let stataSE = stata/SE ;
   %else %let stataSE = ;

 /*-----------------------------------*/
 /*-------Invoking StatTransfer-------*/
 /*-----------------------------------*/
 %if "&sheet" ne "" %then %do;
   x  """&statpath.""
       ""&input_ds_fname.""
       &stataSE.
       ""&output_ds_fname.""
       -y
	  -T<""&sheet""
		" ;

	   %put ----------------------------------------- ;
       %put  %nrstr(%Stattransfer)                    ;
       %put                                           ;
       %put         User: &SYSUSERID.                 ;
       %put         Date: &SYSDATE9.                  ;
       %put                                           ;
       %put         x  "&statpath."                   ;
       %put            "&input_ds_fname."             ;
       %put            &stataSE.                      ;
       %put            "&output_ds_fname."            ;
       %put            /y                             ;
       %put ----------------------------------------- ;

%end;

 %if "&sheet" = "" %then %do;
   x  """&statpath.""
       ""&input_ds_fname.""
       &stataSE.
       ""&output_ds_fname.""
       /y" ;

	   %put ----------------------------------------- ;
       %put  %nrstr(%Stattransfer)                    ;
       %put                                           ;
       %put         User: &SYSUSERID.                 ;
       %put         Date: &SYSDATE9.                  ;
       %put                                           ;
       %put         x  "&statpath."                   ;
       %put            "&input_ds_fname."             ;
       %put            &stataSE.                      ;
       %put            "&output_ds_fname."            ;
       %put            /y                             ;
       %put ----------------------------------------- ;

%end;


 /*---(END) Invocation----------------*/

 /*-----Verifying that the desired file was created */

     filename dirpipe pipe "dir ""&output_ds_fname.""";

     %let file = %upcase(%trim(%left(%scan(%quote(&output_ds_fname.),-1,%str(\)))));

     %if %sysfunc(fileexist(%quote(&output_ds_fname.)))=0 %then %put WAR%str()NING: &file. not created;
     %else %do;

        data _null_;

         infile dirpipe truncover _infile_=fname;
         input;

         fname=upcase(strip(fname));

         if index(fname,"&file.")=0 then delete;

         fname=compbl(fname);
         date1=scan(fname,1," ");
         time1=scan(fname,2," ") || " " || scan(fname,3," ");
         date=input(date1,MMDDYY10.);
         currdate=today();
         time=input(time1,TIME10.);
         currtime=time();
         format date currdate MMDDYY10. time currtime TIME10.;

         if date=currdate and abs(time-currtime)<300 then put "SUC%str()CESS: FILE **&file.** CREATED";
         else do;
           put "WAR%str()NING: File &file. exists, but not created by this program";
           put ">>>>>>> Created: " date time "- Current: " currdate currtime;
         end;

        run;

     %end;

     filename dirpipe clear;

 /*-----(END) file verification */

/*------------------------------------------*/
/*---Skipping here if er/ror is encountered */
/*------------------------------------------*/
%EXIT: ;

options &_mprint. &_xwait. &_xsync. ;

%mend;

****************************************************************************;
*******Program:           more_than_one_per.sas
*******Following Program:
*******Project:           CCSD
*******Description:       Check if byvar1 is associated with more than one byvar2
*******Date Created:      8/9/2014
*******Date updated:      
*******Programmer:        Randy Rosso
*******Notes:             
****************************************************************************;

%macro more_than_one_per(dir=work,indata=,byvar1=,byvar2=,debug=0);

	proc sort data=&dir..&indata. (keep=&byvar1. &byvar2.) nodupkey out=_collapse;
	  by &byvar1. &byvar2.;
	run;

	data _cnt;
		set _collapse;
		by &byvar1. &byvar2.;
		retain _cnt;
		if first.&byvar1. then _cnt=0;
		_cnt+1;
		if last.&byvar1. then output;
	run;

	proc sql;
	    create table _cntmult as
		select _cnt
		from   _cnt
		where _cnt>1;
	quit;

	proc contents data=_cntmult noprint out=_cntmult_contents (keep=nobs);
	run;

   %global dset nobs;
   %let dset=_cntmult;
   %let dsid = %sysfunc(open(&dset));
   %if &dsid %then
      %do;
         %let nobs =%sysfunc(attrn(&dsid,NOBS));
         %let rc = %sysfunc(close(&dsid));
      %end;
   %else %put Open for data set &dset failed - %sysfunc(sysmsg());

	%if &nobs = 0 %then %do;
		%put                                                          ;
		%put PASSED TEST - &nobs obs with multiple observations of &byvar2. associated with single &byvar1.;
		%put                                                          ;
		proc print data=_cntmult_contents (obs=1);
		var nobs;
		title2 "PASSED TEST - 0 obs with multiple observations of &byvar2. associated with single &byvar1.";
		run;
	%end;
	%else %do;
		%put                                                          ;
		%put FAILED TEST - &nobs obs with multiple observations of &byvar2. associated with single &byvar1.;
		%put                                                          ;
			proc print data=_cntmult_contents (obs=1);
			var nobs;
			title2 "FAILED TEST - nobs = # obs with multiple observations of &byvar2. associated with single &byvar1.";
			run;

			proc freq data=_cntmult ;
	            tables _cnt ;
	        run;

			title2 "FAILED TEST - frequency of multiple obs of &byvar2. associated with single &byvar1.";
			run;

			%if &debug.=1 %then %do;
			    proc sql;
			    select &byvar1.,&byvar2.
                from   &dir..&indata.
				where  &byvar1. in (select &byvar1. from _cnt where _cnt>1)
                order by &byvar1.,&byvar2.;
				quit;
			%end;

	%end;
    title2;

%mend;

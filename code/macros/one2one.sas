%macro one2one(dir=work,indata=,idvar1=,idvar2=,debug=0);

proc sql;
    create table dup_&idvar2. as
    select count(distinct &idvar2.) as num_&idvar2., &idvar1.
	from   &dir..&indata.
	group by &idvar1.
    having num_&idvar2.>1;

	create table dup_&idvar1. as
	select count(distinct &idvar1.) as num_&idvar1., &idvar2.
	from   &dir..&indata.
	group by &idvar2.
    having num_&idvar1.>1;
quit;

***DUP IDVAR2 IN TERMS OF IDVAR1***;
proc contents data=dup_&idvar2. noprint out=_&idvar2. (keep=nobs);
run;

%local dset nobs;
   %let dset=dup_&idvar2.;
   %let dsid = %sysfunc(open(&&dset));
   %if &dsid %then
      %do;
         %let nobs =%sysfunc(attrn(&dsid,NOBS));
         %let rc = %sysfunc(close(&dsid));
      %end;
   %else %put Open for data set &dset failed - %sysfunc(sysmsg());

	%if &nobs = 0 %then %do;
		%put                                                          ;
		%put PASSED TEST - &nobs obs with multiple &idvar2.s for single &idvar1. in &dir..&indata.;
		%put                                                          ;
		proc print data=_&idvar2. (obs=1);
		var nobs;
		title2 "PASSED TEST - &nobs obs with multiple &idvar2.s for single &idvar1. in &dir..&indata.";
		run;
	%end;
	%else %do;
		%put                                                          ;
		%put FAILED TEST - &nobs obs with multiple &idvar2.s for single &idvar1. in &dir..&indata.;
		%put                                                          ;
			proc print data=_&idvar2. (obs=1);
			var nobs;
			title2 "FAILED TEST - nobs = # obs with multiple &idvar2.s for single &idvar1. in &dir..&indata.";
			run;

			%if &debug.=1 %then %do;
				proc print data=dup_&idvar2. (obs=10);
				*var &idvar2. &idvar1.;
				title2 "FAILED TEST - first 10 obs with multiple &idvar2.s for single &idvar1. in &dir..&indata.";
				run;
			%end;
	%end;

***DUP IDVAR1 IN TERMS OF IDVAR2***;
proc contents data=dup_&idvar1. noprint out=_&idvar1. (keep=nobs);
run;

%local dset nobs;
   %let dset=dup_&idvar1.;
   %let dsid = %sysfunc(open(&&dset));
   %if &dsid %then
      %do;
         %let nobs =%sysfunc(attrn(&dsid,NOBS));
         %let rc = %sysfunc(close(&dsid));
      %end;
   %else %put Open for data set &dset failed - %sysfunc(sysmsg());

	%if &nobs = 0 %then %do;
		%put                                                          ;
		%put PASSED TEST - &nobs obs with multiple &idvar1.s for single &idvar2. in &dir..&indata.;
		%put                                                          ;
		proc print data=_&idvar1. (obs=1);
		var nobs;
		title2 "PASSED TEST - &nobs obs with multiple &idvar1.s for single &idvar2. in &dir..&indata.";
		run;
	%end;
	%else %do;
		%put                                                          ;
		%put FAILED TEST - &nobs obs with multiple &idvar1.s for single &idvar2. in &dir..&indata.;
		%put                                                          ;
			proc print data=_&idvar1. (obs=1);
			var nobs;
			title2 "FAILED TEST - nobs = # obs with multiple &idvar1.s for single &idvar2. in &dir..&indata.";
			run;

			%if &debug.=1 %then %do;
				proc print data=dup_&idvar1. (obs=10);
				*var &idvar1. &idvar2.;
				title2 "FAILED TEST - first 10 obs with multiple &idvar1.s for single &idvar2. in &dir..&indata.";
				run;
			%end;
	%end;
	title2;
%mend;

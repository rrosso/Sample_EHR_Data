******************************************************************************************;
*******Program:      testmeta.sas 
*******project name: CCSD
*******Description:  This macro compares variables in dataset to list in metadata file
*******Date Created: 2-11-2014
*******Date updated: 
*******Programmer:   Randy Rosso
*******Notes: 
******************************************************************************************;


options nocenter;

/*
%testmeta parameters:
dir= file directory to search for data
mpath= path to metadata file
taskid = name of CCSD data cleaning task
filenm= name of data file
status= specify "raw" or "clean" version of metadata
*/

%macro testmeta(dir=, mpath=, taskid=, filenm=, status=);

%local _stat;
%if %upcase(&status.)=RAW %then %do;
	  %let _stat=RAW;
%end;

title "Testing &status. &taskid. file metadata";

*1. Import metadata spreadsheet to compare to data set;

PROC IMPORT 
DATAFILE="&mpath.\&taskid.&_stat._meta.xlsx"
     OUT=_&taskid.&status._meta
    DBMS=Excel REPLACE;
    getnames=NO;
    range="A2:G100"; 
RUN;

data _&taskid.&status._meta;
    set _&taskid.&status._meta (where=(F1^=""));
	rename F1=varnm
	       F2=varlab
		   F3=vardescript
		   F4=varformat
		   F5=varlength
		   F6=varvalues
		   F7=varnotes
		   ;
run;

/*data _&taskid.&status._meta;
	length varnm $32
	      varlab $75
	      vardescript $150
	      varformat $1
	      varlength 8
	      varvalues $20
	      varnotes $200
	      ;
	infile "&mpath.\&taskid.&_stat._meta.csv" dsd missover firstobs=2;
	input varnm $
	      varlab $
	      vardescript $
	      varformat $
	      varlength
	      varvalues $
	      varnotes $
	      ;

run;*/

proc contents data=&dir..&filenm. out=&filenm.data varnum;
run;

data &filenm.data;
set &filenm.data;
	vid=upcase(Trim(name));
run;

proc sort data=&filenm.data;
by vid;
run;

data &filenm.meta(rename=(varnm=name));
    set _&taskid.&status._meta (keep=varnm);
    where trim(varnm) ^= '';
	vid=upcase(Trim(varnm));
run;

proc sort data=&filenm.meta;
by vid;
run;

data &filenm.test;
merge &filenm.data(in=indata) &filenm.meta(in=inmeta);
by vid;
if indata=1 then in_data=1;
if inmeta=1 then in_meta=1;
if inmeta=1 and indata=1 then in_both=1;
array num _numeric_;
do over num;
	if num=. then num=0;
end;
run;

proc print data=&filenm.test;
where in_data^=1 and in_meta=1;
var  name in_data in_meta;
title2 "BAD: variables in metadata but not in SAS data";
run;

proc print data=&filenm.test;
where in_meta^=1 and in_data=1;
var  name in_data in_meta;
title2 "BAD: variables in SAS data but not in metadata" ;
run;

proc freq data=&filenm.test;
tables in_both * in_data * in_meta/missing list;
title2"freqs of metadata vs. sas data";
run;
title2;


%means_all(dir=&dir.,dsn=&filenm.);

proc sort data=uni_&filenm. ;
by max;
run;

proc print data=uni_&filenm. ;
var varname n nmiss min max mean;
run;

%mend;


%let path = C:\Users\rrosso\Documents\Sample_EHR_Data\data;

libname ehr "C:\Users\rrosso\Documents\Sample_EHR_Data\output";

*** 1. READ IN CSV FILES ***;

data ehr.encounter (drop= yr mo day);
    length encounterid $50 hospid 8. adate $12 los dispid 8. patientid $50 asourceid 8.;
    infile "&path.\encounter.csv" dsd missover firstobs=2;
    input encounterid $
          hospid
          adate $
          los
          dispid
          patientid $
          asourceid;

    yr = substr(adate,1,4);
    mo = substr(adate,6,2);
    day = substr(adate,9,2);
    admit_date = mdy(mo, day, yr);
run;

data ehr.hospital (drop=st_raw);
    length hospid 8. hospitalname $50 st_raw $5;
    infile "&path.\lookuphospital.csv" dsd missover firstobs=2;
    input hospid
          hospitalname $
          st_raw $;

    st = compress(left(trim(st_raw)));
run;

data ehr.state (drop=st_raw);
    length st $2 st_raw $5. statedesc $50;
    infile "&path.\lookupstate.csv" dsd missover firstobs=2;
    input st_raw $
          statedesc $;

    st = compress(left(trim(st_raw)));
run;

data ehr.sex ;
    length sex 8. sexdesc $7;
    infile "&path.\lookupsex.csv" dsd missover firstobs=2;
    input sex
          sexdesc $;
run;

data ehr.race ;
    length raceid 8. racedesc $25;
    infile "&path.\lookuprace.csv" dsd missover firstobs=2;
    input raceid
          racedesc $;
run;

data ehr.diagnosis;
    length dxcode $10 longtitle $256 shorttitle $50;
    infile "&path.\lookupdiagnosis.csv" dsd missover firstobs=2;
    input dxcode $
          longtitle $
          shorttitle $;
run;

data ehr.encounterdx;
    length encounterid $50 seq 8. dxcode $10;
    infile "&path.\encounterdx.csv" dsd missover firstobs=2;
    input encounterid $
          seq
          dxcode $;
run;

data ehr.disposition ;
    length dispid 8 dispdesc $50;
    infile "&path.\lookupdisposition.csv" dsd missover firstobs=2;
    input dispid
          dispdesc $;
run;

data ehr.admitsource;
    length asourceid 8 asourcedesc $50;
    infile "&path.\lookupadmitsource.csv" dsd missover firstobs=2;
    input asourceid
          asourcedesc $;
run;

data patient_raw;
    length patientid $50 fname $50 mname $1 lname $50 address city $75 st $2 zip $5 dob_raw $10 sex raceid 8;
    infile "&path.\patient.csv" dsd missover firstobs=2;
    input patientid $
           fname $
           mname $
           lname $
           address $
           city $
           st $
           zip $
           dob_raw $
           sex
           raceid;
run;

data ehr.patient (drop=mo: yr: day: dob_raw);
    set patient_raw;

    yr = substr(dob_raw,1,4);
    mo = substr(dob_raw,6,2);
    day = substr(dob_raw,9,2);

    dob = mdy(mo, day, yr);

    age = round(yrdif(dob, '01JAN2016'd, 'AGE'));
run;

proc print data=ehr.patient (obs=30);
    var dob age;
    format dob mmddyy10.;
run;

*** 2. MERGE DATA ACCORDING TO ERD ***;
PROC SQL;
    CREATE TABLE ehr.patient_join AS
    SELECT A.*, B.racedesc, C.sexdesc, D.statedesc
    FROM   ehr.patient AS A
             LEFT JOIN
             ehr.race AS B
             ON A.raceid = B.raceid
               LEFT JOIN
               ehr.sex AS C
               ON A.sex = C.sex
                 LEFT JOIN
                 ehr.state AS D
                 ON A.st = D.st;
QUIT;

PROC SQL;
    CREATE TABLE hosp_state AS
    SELECT A.*,B.statedesc
    FROM   ehr.hospital AS A
             LEFT JOIN
           ehr.state AS B
           ON A.st = B.st;
QUIT;

PROC SQL;
    CREATE TABLE diag_enc AS
    SELECT A.*,B.longtitle,B.shorttitle
    FROM   ehr.encounterdx AS A
             LEFT JOIN
           ehr.diagnosis AS B
           ON A.dxcode = B.dxcode;
QUIT;

PROC SQL;
    CREATE TABLE encounter_join AS
    SELECT A.*,B.seq,B.dxcode,B.longtitle,B.shorttitle
    FROM   ehr.encounter AS A
             LEFT JOIN
           diag_enc AS B
           ON A.encounterid = B.encounterid;
QUIT;

PROC SQL;
    CREATE TABLE ehr.encounter_join AS
    SELECT A.*, B.hospitalname, B.st, C.statedesc
    FROM   encounter_join AS A
             LEFT JOIN
           ehr.hospital AS B
           ON A.hospid = B.hospid
             LEFT JOIN
             ehr.state AS C
             ON B.st = C.st;
QUIT;

************************************************************************
** Import the .csv files or restore the Postgre database found at: 
** https://gitlab.com/kandrejk/Sample_EHR_Data/ . 
** You may use a tool of your choice to perform the following:
**
** 1. Create a sample of reports that describe characteristics of the 
** patients. This can include patients demographics, diagnosis and/or 
** any other condition you see fit.
**
** 2. There are several hospitals in this dataset. Create a report 
** that shows their performance. What kind of diseases have they 
** encountered? What can you say about patient throughput, 
** repeat visits, etc.?
**
** 3. As with most raw data it is bound to have errors. 
** Develop a rudimentary data quality check that identifies some 
** of these issues.
**
** 4. Contribute to the documentation: Update or refine the data 
** dictionary, the ERD and/or create new documents that will help 
** others understand the content of the files.
************************************************************************;

libname ehr "C:\Users\rrosso\Documents\Sample_EHR_Data\output";

%include "C:\Users\rrosso\Documents\Sample_EHR_Data\code\macros\testdup.sas";


proc format;
    value age
      24-34 = "24-34"
      35-44 = "35-44"
      45-54 = "45-54"
      55-64 = "55-64"
      65-74 = "65-74"
      75-high = "75 or older";
    value sex
      1 = "Male"
      2 = "Female"
      3 = "Unknown";
    value race
      1 = "White"
      2 = "Black"
      3 = "Hispanic"
      4 = "Asian or Pacific Islander"
      5 = "Native American"
      6 = "Other";
    value disp
      1 = "Routine (Home)"
      2 = "Transfer to Short-term Hospital"
      5	= "Transfer Other: Includes SNF, ICF, Another Type"
      6	= "Home Health Care (HHC)"
      7	= "Against Medical Advice (AMA)"
      20 = "Died"
      99 = "Discharge alive, destination unknown";
    value admit
      1 = "Emergency department"
      2	= "Another hospital"
      3	= "Other health facility including long-term care"
      4	= "Court/Law enforcement"
      5	= "Routine including births and other sources";
run;

proc contents data=ehr.patient_join;
run;

*check for duplicate IDs;
%testdup(dir=ehr,
         filenm=patient_join,
         idvars=patientid,
         keepvars=fname mname lname sex raceid city st,
         print=1)

%testdup(dir=ehr,
         filenm=encounter_join,
         idvars=encounterid seq dxcode,
         keepvars=adate patientid los dispid,
         print=1)

%testdup(dir=ehr,
         filenm=disposition,
         idvars=dispid,
         keepvars=dispdesc,
         print=1)

%testdup(dir=ehr,
         filenm=admitsource,
         idvars=asourceid,
         keepvars=asourcedesc,
         print=1)

%testdup(dir=ehr,
         filenm=hospital,
         idvars=hospid,
         keepvars=hospitalname st,
         print=1)

%testdup(dir=ehr,
         filenm=state,
         idvars=st,
         keepvars=statedesc,
         print=1)

%testdup(dir=ehr,
         filenm=sex,
         idvars=sex,
         keepvars=sexdesc,
         print=1)

%testdup(dir=ehr,
         filenm=race,
         idvars=raceid,
         keepvars=racedesc,
         print=1)

*check for missing IDs;
proc means data=ehr.encounter_join n nmiss;
    var hospid dispid asourceid;
    title "missing values on numeric IDs on encounter table";
run;

title "missing patient/encounter values on encounter table";
proc sql;
    select count(*) as num_miss_patientid
    from   ehr.encounter_join
    where  missing(patientid);

    select count(*) as num_miss_encounterid
    from   ehr.encounter_join
    where  missing(encounterid);
quit;

title "missing patient values on patient table";
proc sql;
    select count(*) as num_miss_patientid
    from   ehr.patient_join
    where  missing(patientid);
quit;
title;

proc sql;
*check for patientid values on encounter table not on patient table;
    select count(*) as num_bad_patient_ids
    from   ehr.encounter_join
    where  patientid not in (select patientid from ehr.patient);

*check for dxcodes on encounterdx not on diagnosis lookup table;
    select count(*) as num_bad_dx_codes
    from   ehr.encounterdx
    where  dxcode not in (select dxcode from ehr.diagnosis);

*check for other IDs on encounter table not on source lookup tables;
    select count(*) as num_bad_disp_ids
    from   ehr.encounter_join
    where  dispid not in (select dispid from ehr.disposition);

    select count(*) as num_bad_admit_ids
    from   ehr.encounter_join
    where  asourceid not in (select asourceid from ehr.admitsource);

    select count(*) as num_bad_hospids
    from   ehr.encounter_join
    where  hospid not in (select hospid from ehr.hospital);

    select count(*) as num_bad_hosp_st
    from   ehr.encounter_join
    where  st not in (select st from ehr.state);

    select count(*) as num_bad_sex_ids
    from   ehr.patient_join
    where  sex not in (select sex from ehr.sex);

    select count(*) as num_bad_raceid
    from   ehr.patient_join
    where  raceid not in (select raceid from ehr.race);
quit;

*investigate bad state IDs on hospital table;
proc sql;
    create table bad_hosp_st as
    select count(*) as num_bad_st, *
    from   ehr.hospital
    where  st not in (select st from ehr.state);
quit;

proc print data=bad_hosp_st (obs=50);
run;
*NOTE: 15 obs with st not on state table are all Puerto Rico hospitals;

  data patient_char;
      set ehr.patient_join;
      count=1;
  run;

  proc sql;
      create table patient_diag as
      select a.*,b.dispid,b.asourceid,b.hospid,b.hospitalname,b.seq,b.dxcode,b.longtitle,b.shorttitle
      from   patient_char as a
                INNER JOIN
             ehr.encounter_join as b
             on a.patientid = b.patientid;
  quit;

  proc sql;
      create table patient_diag_freqs as
      select dxcode, longtitle, shorttitle, sum(count) as num_diag
      from   patient_diag
      group by dxcode;

      create table hospital_diag_freqs as
      select hospid, hospitalname, dxcode, longtitle, shorttitle, sum(count) as num_diag
      from   patient_diag
      group by hospid, dxcode;
  quit;

  proc sort data=patient_diag_freqs (keep=dxcode shorttitle num_diag) nodupkey out=diags;
      by dxcode;
  run;

  proc rank data=diags out=diag_ranks descending ;
      var num_diag;
      ranks diag_rank;
    run;

  proc sql;
      create table pat_ranks as
      select *, sum(num_diag) as sum_diag
      from   diag_ranks
      group by dxcode
      order by dxcode, diag_rank;

      create table pat_ranks_sum as
      select *, sum(sum_diag) as sum_all_diags
      from   pat_ranks
      order by dxcode, diag_rank;
  quit;

  proc sort data=hospital_diag_freqs (keep=hospid hospitalname dxcode longtitle shorttitle num_diag) nodupkey out=hosp_diags;
      by hospid dxcode;
  run;

  proc rank data=hosp_diags out=hosp_diag_ranks descending ;
      var num_diag;
      ranks diag_rank;
      by hospid;
    run;

  proc sort data=hosp_diag_ranks;
      by hospid diag_rank;
  run;

  proc sql;
      create table hosp_ranks as
      select *, sum(num_diag) as sum_diag_by_hosp
      from   hosp_diag_ranks
      group by hospid
      order by hospid, diag_rank;
  quit;

  ods pdf file="C:\Users\rrosso\Documents\Sample_EHR_Data\reports\patient_characteristics.pdf";
  
  proc tabulate data=patient_char missing;
  var count;
  class statedesc sex ;
  tables statedesc="State" all="Total", sex="Sex"*count=""*(sum="Num"*f=comma12.0 rowpctsum="Pct"*f=10.1) count=""*(sum="Total"*f=comma12.0 colpctsum="Col Pct"*f=10.1);
  title "Table 1: Patients by Sex, by State";
  format sex sex.;
  run;

  proc tabulate data=patient_char missing;
  var count;
  class statedesc raceid ;
  tables statedesc="State" all="Total", raceid="Race"*count=""*(sum="Num"*f=comma12.0 rowpctsum="Pct"*f=10.1) count=""*(sum="Total"*f=comma12.0 colpctsum="Col Pct"*f=10.1);
  title "Table 2: Patients by Race, by State";
  format raceid race.;
  run;

  ods pdf close;



  ods pdf file="C:\Users\rrosso\Documents\Sample_EHR_Data\reports\diagnoses.pdf";

  proc tabulate data=patient_diag missing;
  var count;
  class dispid ;
  tables dispid="Disposition" all="Total", count="Patients"*(sum="Num"*f=comma12.0 colpctsum="Pct"*f=10.1);
  title "Table 3: Patients by Disposition";
  format dispid disp.;
  run;

  proc tabulate data=patient_diag missing;
  var count;
  class asourceid ;
  tables asourceid="Admit Source" all="Total", count="Patients"*(sum="Num" colpctsum="Pct");
  title "Table 4: Patients by Admit Source";
  format asourceid admit.;
  run;

  proc report data=pat_ranks_sum nowd split="*" ;
  columns dxcode shorttitle diag_rank sum_diag sum_all_diags pct_total;
  define  dxcode     / display "Dx Code";
  define  shorttitle / display "Diagnosis";
  define  diag_rank / order noprint;
  define  sum_diag   / sum "Total*Incidence" f=comma10.0;
  define  sum_all_diags / sum "Total Diagnoses" f=comma10.0 noprint;
  define  pct_total  / computed "Diag as*% of*Total" f=10.1;
  compute pct_total;
      pct_total = (sum_diag.sum / sum_all_diags.sum) * 100;
  endcomp;
  rbreak after / dol dul summarize;
    compute after;
       shorttitle='TOTAL:';
    endcomp;
  where diag_rank < 6;
  title "Table 5: Number of Patients by Diagnosis Code";
  run;

  proc report data=hosp_ranks split="*" nowd;
  columns hospid hospitalname dxcode shorttitle diag_rank sum_diag_by_hosp num_diag pct_total;
  define  hospid       / order "Hospital*ID";
  define  hospitalname / order "Hospital*Name";
  define  dxcode       / display "Dx Code";
  define  shorttitle   / display "Diagnosis";
  define  diag_rank    / order noprint ;
  define  sum_diag_by_hosp / sum "Total*Diagnoses" f=comma10.0 noprint;
  define  num_diag     / sum "Incidence of Diagnosis" f=comma10.0  ;
  define  pct_total    / computed "Diag as*% of*Hospital*Total" f=10.1;
  compute pct_total;
      pct_total = (num_diag.sum / sum_diag_by_hosp.sum) * 100;
  endcomp;
  
  break after hospid / ol ul summarize;
    compute after hospid;
      num_diag = "Hospital Top 5 Total";
    endcomp;

  rbreak after / dul summarize;
    compute after;
       hospid='TOTAL:';
    endcomp;

  where diag_rank < 6;
  title "Table 6: Top 5 Diagnoses, by Hospital";
  run;

  ods pdf close;
